import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path:'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'result', loadChildren: './result/result.module#ResultPageModule'},
  {path: 'visa', loadChildren: () => import('./visa/visa.module').then( m => m.VisaPageModule)},
  {path: 'feedback', loadChildren: () => import('./feedback/feedback.module').then( m => m.FeedbackPageModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
