import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-visa',
  templateUrl: 'visa.page.html',
  styleUrls: ['visa.page.scss'],
})
export class VisaPage implements OnInit {

  constructor(public router: Router, 
    private quizService: QuizService,
    public activatedRoute: ActivatedRoute){}

    private feedback: string;
    private duration: number;
    private isFinished: string;
    private correctAnswer: number;
    


  ngOnInit() {

    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = "";
  }
  private checkOption(option: number) {
    this.quizService.setOptionSelected();
    if (this.quizService.isCorrectOption(option)) {
      this.feedback =
        this.quizService.getCorrectOptionOfActiveQuestion() +
        ' Oikein !';
    }
    else {
      this.feedback = ' Väärin !';
    }
  }
  private continue() {
    if (this.quizService.isFinished()) {
      this.duration = this.quizService.getDuration();
      this.correctAnswer = this.correctAnswer;
      this.router.navigateByUrl('result/' + this.duration + this.correctAnswer);
    }
    else {
      this.quizService.setQuestion();
    }
  }
}



