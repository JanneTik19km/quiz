import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { QuizService } from '../quiz.service';


@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  constructor(public router:Router,
    private quizService: QuizService,
    public activatedRoute: ActivatedRoute) { }
  
    back_home() {
      this.router.navigateByUrl('home');
    }

  ngOnInit() {

  
    
    
  }

}
