import { Injectable } from '@angular/core';
import { MusaQuestion } from '../musaquestion';


@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: MusaQuestion[] = [];
  private activeQuestion: MusaQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  
  


  constructor() {}



  public addQuestions() {
    this.questions = [{
      question: 'Koska Suomalainen Punk-yhtye Lama on perustettu?',
      options: [
        '1980',
        '1977',
        '1974',
        '1982'],
        correctOption: 1
      },
      {
      question: 'Kuinka monta kertaa Sex Pistols on esiintynyt Suomessa?',
      options: [
        'Kerran',
        'Kahdesti',
        'Kolmesti',
        'Ei kertaakaan',],
        correctOption: 2
      },
      {
        question: 'Montako studioalbumia yhtye nimeltä Rytmihäiriö on tehnyt?',
        options: [
          '6',
          '12 ',
          '4',
          '7',],
          correctOption: 3
        },
        {
          question: 'Kuka on perustanut yhtyeen nimeltä Terveet Kädet?',
          options: [
            'Jari Kronholm',
            'Läjä Äijälä',
            'Kalle Ellilä',
            'Mikko Alatalo',],
            correctOption: 1
          },
          {
            question: 'Kuka heistä on Radiopuhelimet orkesterin laulaja?',
            options: [
              'J.A. Mäki',
              'Ville Laihiala',
              'Juha Tapio',
              'A.W. Yrjänä',],
              correctOption: 0
            }
    ];
  }

  public setQuestion() {
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }

  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  public getQuestions(): MusaQuestion [] {
    return this.questions;
  }

  public getActiveQuestion(): MusaQuestion{
    return this.activeQuestion;
  }

  public getQuestionOfActiveQuestion(): string {
    return this.activeQuestion.question;
  }

  public getOptionsOfActiveQuestion(): string [] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  public getIndexOfOptionCounter(): number {
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    return this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public isCorrectOption(option: number) {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect():boolean {
    return this.isCorrect; 
  }

  public isAnswered():boolean {
    return this.isSelected;
  }

  public isFinished(): boolean {
    
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

  /*public getNumberOfCorrectAnswers(): boolean {
    this.isAnswerCorrect = 
  }*/

}
 


