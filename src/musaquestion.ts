export class MusaQuestion {
    question: string;
    options: string [];
    correctOption: number;
}